################### VPC-1 #################################

variable "cidr_1" {
  description = "cidr value"
  type        = string
  default     = "10.0.0.0/16"
}

variable "tenancy_1" {
  description = "instance tenancy"
  type        = string
  default     = "default"
}

variable "vpc_tag_1" {
  description = "vpc tag"
  type        = string
  default     = "vpc-01"
}

################### VPC-2 ###################################

variable "cidr_2" {
  description = "cidr value"
  type        = string
  default     = "11.0.0.0/16"
}

variable "tenancy_2" {
  description = "instance tenancy"
  type        = string
  default     = "default"
}

variable "vpc_tag_2" {
  description = "vpc tag"
  type        = string
  default     = "vpc-02"
}

####################### VPC-1-SUBNET ###########################

variable "vpc_1_subnet_cidrs" {
  type        = string
  description = "Public Subnet CIDR values"
  default     = "10.0.1.0/24"
}

variable "vpc_1_sub_tag" {
  type    = string
  default = "vpc_1_sub"
}

######################## VPC-2-SUBNET ###########################

variable "vpc_2_subnet_cidrs" {
  type        = string
  description = "vpc_2 Subnet CIDR values"
  default     = "11.0.1.0/24"
}

variable "vpc_2_sub_tag" {
  type    = string
  default = "vpc_2_sub"
}

########################### VPC-1-IGW #############################

variable "vpc_1_igw_tag" {
  type        = string
  description = "vpc1 internet gateway"
  default     = "vpc_1-igw"
}

############################# VPC-2-IGW ############################

variable "vpc_2_igw_tag" {
  type        = string
  description = "vpc2 internet gateway"
  default     = "vpc_2-igw"
}

################################ NACL ##############################

variable "destinationCIDRblock" {
  type    = string
  default = "0.0.0.0/0"
}

##################### Routing table for VPC-1 ######################

variable "vpc-1_rtb" {
  type        = string
  description = "VPC-1 route table"
  default     = "VPC-1-route-table"
}

variable "vpc-1_igw-1_cidr" {
  type        = string
  description = "public rtb entry to igw"
  default     = "0.0.0.0/0"

}
###################### Routing table for VPC-2 #######################

variable "vpc-2_rtb" {
  type        = string
  description = "VPC-1 route table"
  default     = "VPC-1-route-table"
}

variable "vpc-2_igw-2_cidr" {
  type        = string
  description = "public rtb entry to igw"
  default     = "0.0.0.0/0"

}

########################## KEY-PAIR ####################################

variable "key-algorithm" {
  type        = string
  description = "key algorithm"
  default     = "RSA"
}

variable "key-filename" {
  type        = string
  description = "key filename"
  default     = "travel_creator.pem"
}

variable "key-name" {
  type        = string
  description = "file name"
  default     = "travel_creator"
}

########################## VPC-1_EC2 #############################

variable "vpc-1_ec2_ami" {
  type        = string
  description = "public ec2 ami"
  default     = "ami-0557a15b87f6559cf"
}

variable "vpc-1_ec2_ins_type" {
  type        = string
  description = "vpc-1 instance type"
  default     = "t2.micro"
}

variable "vpc-1-ec2-server" {
  type        = string
  description = "vpc-1 instance"
  default     = "vpc-1-Ubuntu"
}

variable "vpc-1_iam_role" {
  type        = string
  description = "attach iam role"
  default     = "ec2"
}


############################# VPC-2-EC2 #############################

variable "vpc-2_ec2_ami" {
  type        = string
  description = "private ec2 ami"
  default     = "ami-0c9978668f8d55984"
}

variable "vpc-2_ec2_ins_type" {
  type        = string
  description = "vpc-2 instance type"
  default     = "t2.micro"
}

variable "vpc-2-ec2-server" {
  type        = string
  description = "vpc-2 instance"
  default     = "vpc-2_RedHat"
}

variable "vpc-2_iam_role" {
  type        = string
  description = "attach iam role"
  default     = "ec2"
}


############################### SG ####################################

variable "vpc-1_sg" {
  description = "Security group name"
  type        = string
  default     = "vpc-1_sg"
}

variable "vpc-2_sg" {
  description = "Security group name"
  type        = string
  default     = "vpc-1_sg"
}

