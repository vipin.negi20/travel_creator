######################## VPC-1 ##############################

output "vpc_id-1" {
  value       = aws_vpc.vpc_1.id
  description = "My VPC-1 ID"
}

######################## VPC-2 ##############################

output "vpc_id-2" {
  value       = aws_vpc.vpc_2.id
  description = "My VPC-2 ID"
}

###################### VPC-1-SUBNET #########################

output "vpc-1_sub" {
  value       = aws_subnet.vpc_1_subnet.id
  description = "vpc-1 subnet"
}

####################### VPC-2-SUBNETS ########################

output "vpc-2_sub" {
  value       = aws_subnet.vpc_2_subnet.id
  description = "vpc-2 subnet"
}

######################### VPC-1-IGW ##########################

output "vpc-1-igw" {
  value       = aws_internet_gateway.igw-1.id
  description = "internet gateway"

}

######################### VPC-2-IGW ##########################

output "vpc-2-igw" {
  value       = aws_internet_gateway.igw-2.id
  description = "internet gateway"

}

################### Routing table for VPC-1 ##################

output "vpc-1-rtb" {
  value       = aws_route_table.vpc-1_rtb.id
  description = "vpc-1 route table"
}

################### Routing table for VPC-2 ##################

output "vpc-2-rtb" {
  value       = aws_route_table.vpc-2_rtb.id
  description = "vpc-2 route table"
}

######################## VPC-1_EC2 ############################

output "vpc-1_EC2" {
  value       = aws_instance.vpc-1_EC2.id
  description = "Ubuntu Server"
}

########################## VPC-2_EC2 ###########################

output "vpc-2_EC2" {
  value       = aws_instance.vpc-2_EC2.id
  description = "private server"
}

############################ VPC-1_SG ##########################

output "vpc-1_sg" {
  value       = aws_security_group.vpc-1_sg.id
  description = "VPC-1 Security Group"
}

############################ VPC-2_SG ##########################

output "vpc-2_sg" {
  value       = aws_security_group.vpc-2_sg.id
  description = "VPC-2 Security Group"
}