######################### VPC-1 ##############################

resource "aws_vpc" "vpc_1" {
  cidr_block       = var.cidr_1
  instance_tenancy = var.tenancy_1

  tags = {
    Name = var.vpc_tag_1
  }
}

######################### VPC-2 ##############################

resource "aws_vpc" "vpc_2" {
  cidr_block       = var.cidr_2
  instance_tenancy = var.tenancy_2

  tags = {
    Name = var.vpc_tag_2
  }
}

##################### VPC-1-SUBNET ###########################

resource "aws_subnet" "vpc_1_subnet" {
  vpc_id     = aws_vpc.vpc_1.id
  cidr_block = var.vpc_1_subnet_cidrs

  tags = {
    Name = var.vpc_1_sub_tag
  }
}

######################## VPC-2-SUBNETS ########################

resource "aws_subnet" "vpc_2_subnet" {
  vpc_id     = aws_vpc.vpc_2.id
  cidr_block = var.vpc_2_subnet_cidrs

  tags = {
    Name = var.vpc_2_sub_tag
  }
}

######################### VPC-1-IGW ############################

resource "aws_internet_gateway" "igw-1" {
  vpc_id = aws_vpc.vpc_1.id

  tags = {
    Name = var.vpc_1_igw_tag
  }
}

######################### VPC-2-IGW ############################

resource "aws_internet_gateway" "igw-2" {
  vpc_id = aws_vpc.vpc_2.id

  tags = {
    Name = var.vpc_2_igw_tag
  }
}


######################### VPC-1-NACL ############################
resource "aws_network_acl" "VPC_1_Security_ACL" {
  vpc_id     = aws_vpc.vpc_1.id
  subnet_ids = [aws_subnet.vpc_1_subnet.id]
  # allow ingress port 22
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 22
    to_port    = 22
  }

  # allow ingress port 80 
  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 80
    to_port    = 80
  }

  # allow ingress ephemeral ports 
  ingress {
    protocol   = "tcp"
    rule_no    = 900
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 1024
    to_port    = 65535
  }

  # allow egress port 22 
  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 22
    to_port    = 22
  }

  # allow egress port 80 
  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 80
    to_port    = 80
  }

  # allow egress ephemeral ports
  egress {
    protocol   = "tcp"
    rule_no    = 900
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 1024
    to_port    = 65535
  }
  tags = {
    Name = "VPC-1_NACL"
  }
}

######################### VPC-2-NACL ############################
resource "aws_network_acl" "VPC_2_Security_ACL" {
  vpc_id     = aws_vpc.vpc_2.id
  subnet_ids = [aws_subnet.vpc_2_subnet.id]
  # allow ingress port 22
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 22
    to_port    = 22
  }

  # allow ingress port 80 
  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 80
    to_port    = 80
  }

  # allow ingress ephemeral ports 
  ingress {
    protocol   = "tcp"
    rule_no    = 900
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 1024
    to_port    = 65535
  }

  # allow egress port 22 
  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 22
    to_port    = 22
  }

  # allow egress port 80 
  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 80
    to_port    = 80
  }

  # allow egress ephemeral ports
  egress {
    protocol   = "tcp"
    rule_no    = 900
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 1024
    to_port    = 65535
  }
  tags = {
    Name = "VPC-2_NACL"
  }
}

################### Routing table for VPC-1 #####################

resource "aws_route_table" "vpc-1_rtb" {
  vpc_id = aws_vpc.vpc_1.id
  tags = {
    Name = var.vpc-1_rtb
  }
}

resource "aws_route" "vpc-1_rtb_igw-1" {
  route_table_id         = aws_route_table.vpc-1_rtb.id
  destination_cidr_block = var.vpc-1_igw-1_cidr
  gateway_id             = aws_internet_gateway.igw-1.id
}

resource "aws_route_table_association" "vpc-1_subs" {
  subnet_id      = aws_subnet.vpc_1_subnet.id
  route_table_id = aws_route_table.vpc-1_rtb.id
}

################## Routing table for VPC-2 ########################

resource "aws_route_table" "vpc-2_rtb" {
  vpc_id = aws_vpc.vpc_2.id
  tags = {
    Name = var.vpc-2_rtb
  }
}

resource "aws_route" "vpc-2_rtb_igw-2" {
  route_table_id         = aws_route_table.vpc-2_rtb.id
  destination_cidr_block = var.vpc-2_igw-2_cidr
  gateway_id             = aws_internet_gateway.igw-2.id
}

resource "aws_route_table_association" "vpc-2_subs" {
  subnet_id      = aws_subnet.vpc_2_subnet.id
  route_table_id = aws_route_table.vpc-2_rtb.id
}

########################### KEY-PAIR ###############################

resource "tls_private_key" "rsa" {
  algorithm = var.key-algorithm
  rsa_bits  = 4096
}
resource "local_file" "key" {
  filename = var.key-filename
  content  = tls_private_key.rsa.private_key_pem
}
resource "aws_key_pair" "key_pair" {
  key_name   = var.key-name
  public_key = tls_private_key.rsa.public_key_openssh
}

############################# VPC-1_EC2 #############################

resource "aws_instance" "vpc-1_EC2" {
  ami                         = var.vpc-1_ec2_ami
  instance_type               = var.vpc-1_ec2_ins_type
  key_name                    = var.key-name
  subnet_id                   = aws_subnet.vpc_1_subnet.id
  vpc_security_group_ids      = [aws_security_group.vpc-1_sg.id]
  associate_public_ip_address = true
  iam_instance_profile        = var.vpc-1_iam_role

  tags = {
    "Name" = var.vpc-1-ec2-server
  }
}

############################# VPC-2-EC2 #############################

resource "aws_instance" "vpc-2_EC2" {
  ami                         = var.vpc-2_ec2_ami
  instance_type               = var.vpc-2_ec2_ins_type
  key_name                    = var.key-name
  subnet_id                   = aws_subnet.vpc_2_subnet.id
  vpc_security_group_ids      = [aws_security_group.vpc-2_sg.id]
  associate_public_ip_address = true
  iam_instance_profile        = var.vpc-2_iam_role

  tags = {
    "Name" = var.vpc-2-ec2-server
  }
}

############################ VPC-1_SG ###############################

resource "aws_security_group" "vpc-1_sg" {
  vpc_id = aws_vpc.vpc_1.id
  name   = var.vpc-1_sg

  ingress {
    description = "Port-22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    description = "Port-80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    description = "Port-443"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    description = "ICMP"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }
}

############################ VPC-2_SG ###############################

resource "aws_security_group" "vpc-2_sg" {
  vpc_id = aws_vpc.vpc_2.id
  name   = var.vpc-2_sg

  ingress {
    description = "Port-22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    description = "Port-80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    description = "Port-443"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    description = "ICMP"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]


  }
}